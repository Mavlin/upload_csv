<?php

namespace Controllers;

class Ob extends Controller //
{
    const
        COUNT_OB_IN_PAGE = 50, // кол-во объявлений на странице
        BODY_MAX_LENGTH = 200,
        CONTACTS_MAX_LENGTH = 80;
    protected $Ob; // ссылка на модель

    public function __construct( $req ){
        parent::setCfg();
        $this->Ob = new \Models\Ob( $this->config );
        if ($req !== null) {
            parent::__construct( $req );
        }
    }

    public function _save_(){
        sleep (0);
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $data = $this->getRequestParam( $this->request, 'data');
        $fileName = $this->getRequestParam( $this->request,'fileName');
// http://phpfaq.ru/safemysql/examples#multiinsert

        $ins = array(); $result = false;
        $except = [];
        if (is_array($data)) {
            for ($i = 0, $size = count($data); $i < $size; ++$i) {
                $body = $data[$i][0];
                $contacts = $data[$i][1];
                if ( iconv_strlen($body, 'utf-8') > $this::BODY_MAX_LENGTH ||
                    iconv_strlen($contacts, 'utf-8') > $this::CONTACTS_MAX_LENGTH ){
                    $j = $i; // чтобы сохранить привычную нумерацию с 1
                    $except[] = ++$j;
                } else { // парсим массив
                    $ins[] = $this->Ob->parseData( $body, $contacts );
//                    $ins[] = $this->Ob->parseData( htmlspecialchars( $body ), htmlspecialchars( $contacts ));
                }
            }
            if (!empty($ins)){ // добавляем данные
                $result = $this->Ob->addOb( $ins );
                // обновим общее кол-во объялений
                $this->Ob->updCount();
            }
        }
        if (!empty($except)){
            $resp['fileName'] = $fileName;
            $resp['rows'] = $except;
            $rc = $this->hasIncorrectRow;
            $this->setResponse('data', $resp);
        } elseif ( $result ){
            $rc = $this->success;
        } else {
            $rc = $this->SomethingWrong;
        }
        $this->setResponse('code', $rc['code']);
        $this->setResponse('message', $rc['message']);
        $this->setResponse('status', 'ok');
    }

    public function _page_(){

        $idPage = (int)$this->getRequestParam( $this->request[(string)__FUNCTION__]['data'], 'id');
        $obCount = $this->Ob->getCountOb();
        $firstID = $this->Ob->getFirstID();
        // индекс об-я ранее которого смотреть не нужно
        $idOb = $obCount - (($idPage-1) * $this::COUNT_OB_IN_PAGE);

        $data['ob_data'] = $this->Ob->getPage( $firstID + $idOb, $this::COUNT_OB_IN_PAGE );
        $data['ob_in_group'] = $this::COUNT_OB_IN_PAGE;
        $data['ob_count'] = $obCount;

        $this->setResponse('data', $data);
        $this->setResponse('code', 0);
        $this->setResponse('status', 'ok');
    }
    // полная очистка таблицы с об-ми
    public function _purgeDB_(){
        $result = $this->Ob->purgeOb();
        $obCount = $this->Ob->getCountOb();
        $data['ob_count'] = $obCount;

        if ( $result ){
            $rc = $this->purgeDB;
        } else {
            $rc = $this->SomethingWrong;
        }
        $this->setResponse('data', $data);
        $this->setResponse('code', $rc['code']);
        $this->setResponse('message', $rc['message']);
    }

}
