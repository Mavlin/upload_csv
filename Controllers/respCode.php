<?php

namespace Controllers;
/* содержит коды ответов
 * */

trait respCode
{
    public $dataNotFound = [
        'code' => 3,
        'message' => 'Данных нет'
    ];
    public $success = [
        'code' => 0,
        'message' => 'Все идет по плану :-))'
    ];
    public $hasIncorrectRow = [
        'code' => 2,
        'message' => 'часть строк не записана'
    ];
    public $purgeDB = [
        'code' => 5,
        'message' => 'таблица данных очищена'
    ];
    public function dataNotValid( $key )
    {
        return [
            'code' => 12, //
            'message' => 'данные для поля ' . $key . ' не валидны :('
        ];
    }
    public $noField = [
        'code' => 24,
        'message' => "Требуемое поле не заполнено: "
    ];
    public $SomethingWrong = [
        'code' => 41,
        'message' => 'Something went wrong'
    ];
}
