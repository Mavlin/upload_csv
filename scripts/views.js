"use strict";
//console.time("All Benchmarks");
(function () {
    let Views = {};
    /**
     * возвращает шаблон компонента
     *
     * @constructor
     * @param {object} arg
     * @param {null} place
     * @return {template}
     * */
    Views.Place = function ( arg, place = null ){
        this.prop = {
            id: arg.id,
            class: arg.class || '',
            html: arg.html || '',
            mark: arg.mark || '', // поле нужно для обновления компонента
            parent: arg.parent,
            where: arg.where,
            link: arg.link || null,
            visible: arg.visible || null,
            hidden: arg.hidden || null
        };
        if (place) this.place = place;
        this.template = "<div id={{id}} {{mark}} data-visible={{visible}} {{hidden}} data-a={{link}} class={{class}}>{{html}}</div>";
    };
    Views.Place.prototype = Object.create( A.My.prototype );
    Views.Place.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };

    /**
     * форма для загрузки объявлений
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.ObAddForm = function ( arg, place ){
        this.prop = {
            id: arg.id || A.ID.ObAdd,
            class: A.SEL.textToSend,
            html: arg.html
        };
        this.prop.id_btn = this.prop.id + '_' + A.ID.sendButton;
        this.prop.id_contacts = this.prop.id + '_' + A.ID.contacts;
        this.place = place;
        this.template = "{{html}}";
        this.setHtml();
    };
    Views.ObAddForm.prototype = Object.create( A.My.prototype );
    Views.ObAddForm.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };
    /**
     * представление для выбранного файла для отправки
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.FilesItem = function ( arg, place ){
        this.prop = {
            id: arg.data.name.substring(0, arg.data.name.length - 4),
            class: A.SEL.textToSend + ' ' + A.SEL.readyToSend,
            name: ()=>{let name = arg.data.name; return (name.length>40) ? name.substring(0, 40) + ' ...' : name},
            size: (arg.data.size/1024).toFixed(1) + ' Кбайт',
            html: arg.html
        };
        this.place = place;
        this.template = "<li id={{id}} class={{class}} data-name={{name}}>" +
            "<a class='cancelItem a' hidden data-a='{\"select\":\"common\",\"mode\":\"remove\"}'>&#xe6fb;</a>" +
            "<div class='case_Fitem'>"+
            "<canvas class='progress'></canvas>" +
            "<textarea hidden>{{html}}</textarea>" +
            "<i class=main>&#xe61a;</i></div><div><div>{{name}}</div><div>{{size}}</div></div>" +
            "</li>";
    };
    Views.FilesItem.prototype = Object.create( A.My.prototype );
    Views.FilesItem.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };

    /**
     * блок для вывода исключений
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.ExceptionsItem = function ( arg, place ){
        this.prop = {
            id: arg.id,
            name: arg.data.fileName,
            rows: arg.data.rows
        };
        this.place = place;
        this.template = "<div id={{id}} class='except'>" +
            "{{name}}.csv содержит строки {{rows}} размеры полей которых выходят за пределы, они пропущены.</div>";
    };
    Views.ExceptionsItem.prototype = Object.create( A.My.prototype );
    Views.ExceptionsItem.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };

    /**
     * pagination item
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.PaginationItem = function ( arg, place ){
        this.prop = {
            id: arg.id,
            active: arg.active,
            html: arg.html,
        };
        this.place = place;
        this.template = "<div id={{id}} data-a='{\"select\":\"ob\",\"mode\":\"page\",\"data\":{\"id\":\"{{html}}\"}}'" +
            " class='a pageItem {{active}}'>{{html}}</div>";
    };
    Views.PaginationItem.prototype = Object.create( A.My.prototype );
    Views.PaginationItem.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };

    /**
     * Page view
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.Page = function ( arg, place ){
        this.prop = {
            id: A.ID.page + arg.id,
            date: function () {
                let date = new Date( arg.data.date );
                let options = {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                };
                return date.toLocaleString("ru", options); // среда, 31 декабря 2014 г. н.э. 12:30:00
            },
            body: arg.data.body,
            contacts: arg.data.contacts,
        };
        this.place = place;
        this.template = "<div id={{id}} class='obItem'>" +
            "<div class='date'>{{date}}</div><div class='body'>{{body}}</div><div class='contacts'>{{contacts}}</div>" +
            "</div>";
    };
    Views.Page.prototype = Object.create( A.My.prototype );
    Views.Page.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };

    /**
     * side btn view
     *
     * @param {object} arg
     * @param {string} place
     *
     * */
    Views.SideBtn = function ( arg, place ){
        this.prop = {
            id: arg.id,
            html: arg.html,
            direction: arg.direction,
            disabled: arg.state
        };
        this.place = place;
        this.template = '<a id={{id}} href="#{{direction}}" disabled={{disabled}} class="a sideBtn" ' +
            'data-a=\'{"select":"ob","mode":"page","data":{"id":"{{direction}}"}}\'>{{html}}</a>';
    };
    Views.SideBtn.prototype = Object.create( A.My.prototype );
    Views.SideBtn.prototype.render = function(){
        this.setHtml();
        this.setParentComp();
        this.setRenderStatus();
    };


    /**
     * рисование
     *
     * @param {HTMLElement} canvasid
     * @param {number} width
     * @param {number} height
     * */
    function ExpangeCanvas( canvasid, width, height ){
        width = width || 300;
        height = height || 150;
        this.canvas = canvasid;
        this.obCanvas = null;
        if ( this.canvas !== null ){
            this.canvas.width = width;
            this.canvas.height = height;
            this.obCanvas = this.canvas['getContext']('2d');
        }
        this.Line = function ( x1, y1, x2, y2, linewidth, strokestyle ){
            if (this.obCanvas === null) return;
            this.obCanvas.beginPath();
            this.obCanvas.lineWidth = linewidth;
            this.obCanvas.strokeStyle = strokestyle;
            this.obCanvas.moveTo(x1, y1);
            this.obCanvas.lineTo(x2, y2);
            this.obCanvas.stroke();
        };
        this.Circle = function ( x, y, radius, linewidth, strokestyle, lineDash, fillstyle, val ){
            if (this.obCanvas === null) return;
            fillstyle = fillstyle || 'transparent';
            this.obCanvas.beginPath();
            this.obCanvas.arc(x, y, radius, 0, 2 * Math.PI * 0.01 * val, false);
            this.obCanvas.lineWidth = linewidth;
            this.obCanvas['setLineDash']( lineDash || [0,0] );
            this.obCanvas.strokeStyle = strokestyle;
            this.obCanvas.fillStyle = fillstyle;
            this.obCanvas.fill();
            this.obCanvas.stroke();
        };
        this.Text = function ( text, x, y, font, color ){
            if (this.obCanvas === null) return;
            this.obCanvas.font = font;
            this.obCanvas.fillStyle = color;
            this.obCanvas.textAlign = 'center';
            this.obCanvas.textBaseline = "middle";
            this.obCanvas.fillText(text, x, y);
        };
        this.Text2 = function ( x, y ){
            if (this.obCanvas === null) return;
            this.obCanvas.font = '25px Glyphicons Halflings';
            this.obCanvas.fillStyle = '#95E2FF';
            this.obCanvas.textAlign = 'center';
            this.obCanvas.textBaseline = "middle";
            console.log('\e013');
            this.obCanvas.fillText('\e013', x, y);
        };
        this.SetBgColor = function ( bgcolor ){
            if (this.obCanvas === null) return;
            this.obCanvas.fillStyle = bgcolor;
            this.obCanvas.fillRect(0, 0, this.obCanvas.canvas.width, this.obCanvas.canvas.height);
        }
    }

    A.ExpangeCanvas = ExpangeCanvas;
    A.Views = Views;

})();

