"use strict";
//console.time("All Benchmarks");
(function () {

    let Ets = A.Ets;

    let act = {
        // вывод определенной страницы
        ob_page: [
            {f:()=>Ets.getPage},
            {f:()=>Ets.setHash},
            {f:()=>Ets.setReqFromURL},
            {f:()=>Ets.fetch},
            {f:()=>Ets.showResp},
            {f:()=>Ets.renderPagination},
            {f:()=>Ets.renderSideBtn},
            {f:()=>Ets.renderPage},
            {f:()=>Ets.renderDOM},
            {f:()=>Ets.removeComp},
        ],
        // Задание №1
        task_one: [
            {f:()=>Ets.setHash},
            {f:()=>Ets.getFile, d:'/blocks/taskOne.html'},
            {f:()=>Ets.taskOne},
            {f:()=>Ets.removeComp}
        ],
        // new ob
        ob_new: [
            {f:()=>Ets.setHash},
            {f:()=>Ets.getFile, d:'/blocks/obAdd.php'},
            {f:()=>Ets.obAdd},
            {f:()=>A.getScript, d:'/scripts/papaparse.min.js'},
            {f:()=>A.getScript, d:'/scripts/sortable.min.js'},
            {f:()=>Ets.removeComp},
        ],
        // change file input
        common_file_input: [
            {f:()=>Ets.showImg},
            {f:()=>Ets.enableSendButt}
        ],
        common_changeVisiblePgn: [
            {f:()=>Ets.changeVisible, d:A.ID.obPaginationCaseId},
            ],
        common_remove: [
            {f:()=>Ets.removeItem},
            {f:()=>Ets.enableSendButt}
            ],
        // save ob
        ob_save: [
            {f:()=>Ets.disableSendButt},
            {f:()=>Ets.showLoader},
            {f:()=>Ets.disableForm},
            {f:()=>Ets.sendCsv},
            {f:()=>Ets.showResp},
            {f:()=>Ets.enableForm},
            {f:()=>Ets.disableSendButt},
            {f:()=>Ets.removeLoader}
        ],
        // очистка данных
        ob_purgeDB: [
            {f:()=>Ets.setReqFromURL},
            {f:()=>Ets.fetch},
            {f:()=>Ets.showResp},
        ]
    };

    Object.assign( A.actions, act );

})();

