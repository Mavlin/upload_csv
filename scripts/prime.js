"use strict";
//console.time("All Benchmarks");
(function () {
    let
        actions,
        dev = A.dev,
        exp = {},// содержит имена кодов для клиента
        arg = {}; // глобальный хранитель и переносчик данных

    exp.type = {
        1: 'список',
        2: 'строка',
        3: 'число',
        4: 'логическое'
    };
    arg.cs = {};// коды состояний
    /**
     * проверяет наличие элемента в DOM
     *
     * @param {string} a
     * @throws Will throw an error if the result is null.
     */
    function checkEl( a ){
        if (gid( a ) === null) throw new Error('блок '+ a +' не найден((');
    }
    /**
     * возвращает ссылку на элемент
     *
     * @param {string} a
     * @return {Element}
     */
    function gid( a ){
        return document.getElementById( a )
    }
    /**
     * содержит CSS селекторы
     *
     * @return {string}
     */
    let SEL = Object.create( null );// объект хранит селекторы блоков, что исп-ся в различных модулях ( C = conf )
    SEL = {
        obNew: '.ob_new',
        textToSend: 'textToSend', // класс текстовых полей для отправки
        readyToSend: 'readyToSend', // класс файлов для отправки
        sendToComplete: 'sendToComplete', // класс для отправленных файлов
        active: 'active', // класс, селектор активных ссылок и мехов акктордиона
        err: 'err', // класс уведомлений об ошибках
        pre: 'pre', // класс предварительно скрытых элементов (например до сохранения об-я)
        owner: 'owner' // класс 'k-d доступных владельцам материалов
    };
    /**
     * содержит цвета
     *
     * @return {string}
     */
    let Color = {
            text: '#056ab1'
        };
    /**
     * содержит сокращения для местоположения нового элемента
     *
     * @return {string}
     */
    let Where = Object.create( null );
    Where = {
        1: 'beforeBegin',
        2: 'afterBegin',
        3: 'beforeEnd',
        4: 'afterEnd'
    };

    /**
     * об-т для общих действий http://javascript.ru/tutorial/object/inheritance#polnyy-primer-nasledovaniya
     *
     * @constructor
     * */
    let Common = function(){};

    /**
     * select element by css
     *
     * @param {string} s
     * @param {html} context
     * @return {object} this
     * */
    Common.prototype.select = function( s, context ){
        if (s) {
            let arr = (context || document).querySelectorAll(s);
            if ( arr.length ) this.div = arr;
            return this;
        }
    };
    /**
     * show loader
     * https://learn.javascript.ru/multi-insert#добавление-множества-узлов
     *
     * param {string} sel
     *
     * */
    Common.prototype.loader = function( sel ){
        let el = sel;
        let show = function (){
            let flagLoader = el.dataset.loader;
            let style = getComputedStyle( el );
            el.style.minWidth = parseFloat(style.width) + 'px';
            el.dataset.temp = el.innerHTML;
            if ( flagLoader === 0 || flagLoader === undefined ){
                el.dataset.loader = 1;
                el.innerHTML = ('<span></span>');
                let char = '&#xe70a;';
                let interval = 300;
                let i = 0;
                let loadr = el.querySelector('span');
                setTimeout(function run() {
                    if (loadr) {
                        loadr.insertAdjacentHTML('beforeEnd', char);
                        i += 1;
                        if (i > 3) {
                            loadr.innerHTML = '';
                            i = 0;
                        }
                        setTimeout( run, interval );
                    }
                }, interval);
            }
        };
        let remove = function (){
            let e = el.querySelector('span');
            if ( e ) e.remove();
            el.dataset.loader = 0;
            el.innerHTML = el.dataset.temp;
        };
        return {
            show: show,
            remove: remove
        };
    };

    /**
     * объект для работы с формами
     *
     * @param {string || HTMLElement} el
     * @set {this}
     * */
    let Form = function ( el ) {
        if ( el instanceof HTMLElement ){
            this.form = el;
        } else {
            this.form = A.gid( el );
        }
        this.valid = 0;
        this.submitButt = this.form.querySelector('button[type="submit"]');
        this.inputs = this.form.querySelectorAll('.' + A.SEL.textToSend);
        this.fileInput = this.form.querySelector('input[type=file]');
        this.inputsAll = this.form.querySelectorAll('input, textarea, button, select');
        this.files = this.form.querySelectorAll('.' + A.SEL.readyToSend);
        this.data = {};
        this.name = this.form.dataset.name;
    };
    /**
     * управляет состоянием кнопки отправки
     *
     * @this Form
     * @set {this}
     * */
    Form.prototype.submitButtonState = function (){
        let ss = this.submitButt;
        let sb = function () {
            if (!ss) return false;
        };
        let enabled = function (){
            if (sb) {
                ss.disabled = 0;
            }
        };
        let disabled = function (){
            if (sb) {
                ss.disabled = 1;
            }
        };
        return {
            enabled: enabled,
            disabled: disabled
        };
    };
    /**
     * блокировка формы, на время отправки данных
     *
     * @param {number} $key
     * @this Form
     * @set {this}
     * */
    Form.prototype.state = function ( $key ){
        let arr = this.inputsAll, item;
        for ( let i = 0; i < arr.length; ++i ){
            item = arr[ i ];
            if (item.classList.contains('save')) return;
            item.disabled = $key
        }
    };
    /**
     * формирование массива данных с формы
     *
     * @this Form
     * @set {object} this.data
     * */
    Form.prototype.getData = function (){
        let data = {};
        let arr = this.inputs, item;
        for ( let i = 0; i < arr.length; ++i ) {
            item = arr[ i ];
            //console.log ( item.dataset.name + ' - ' +  + ' == ' + item.innerHTML );
            if ((item.name || item.dataset.name) && item.innerHTML) {
                switch (item.tagName) { // для DOM элементов
                    case 'P':
                    case 'DIV':
                    case 'SPAN':
                        data[item.dataset.name] = item.innerHTML;
                        break;
                    case 'SELECT':
                        data[item.name || item.dataset.name] = item.value;
                        break;
                    case 'UL':
                        /**
                         * todo me
                         * чтобы использовать список, нужно ему присвоить класс для отправки данных
                         * датасет name, а элементам списка датасет id
                         * */
                            //console.log ( item.tagName +' ---- '+ item.innerHTML );
                            //console.log ( item.name + ' --- ' + item.value );
                            //console.log ( item.innerHTML );
                        let lis = item.childNodes, ar = [];
                        for (let i = 0; i < lis.length; ++i) {
                            let id = lis[i].dataset.id || lis[i].dataset.name;
                            if (lis.hasOwnProperty(i) && id) ar = ar.concat(id)
                        }
                        if (lis.length) data[item.dataset.name] = ar;
                }
            }
            if (item.type === 'checkbox' && item.checked) data[item.name] = 1;
            if (item.type === 'checkbox' && item.checked === false) data[item.name] = 0;
            if (item.type === 'radio' && item.checked) data[item.name] = item.value;
            switch (item.type) {
                case 'text':
                case 'password':
                case 'email':
                case 'textarea':
                    if (item.dataset.type === 'number') data[item.name] = item.value.replace(/\s+/g, '');
                    else if (item.value) data[item.name] = item.value;
                    break;
                case 'file':
                    break;
            }
        }
        this.data = data;
        // console.log ( data );
        return this.data;
    };
    /**
     * вывод изображений формы
     *
     * @param {Blob} data
     * @param {string} key
     *
     * @this Form
     * @set {this}
     * */
    Form.prototype.setFileData = function ( data, key, resolve = null ){
        // debugger
        let addItem = function (i, el) {
            let li = document.createElement('LI');
            if (i.constructor === Object) {
                li.innerHTML = '<img src=' + i['dtf_path'] + ' width="100%" >';
                li.classList.add( A.SEL.sendToComplete );
                li.dataset.name = i[ 'dtf_name' ];
                let ok = document.createElement('a');
                ok.classList.add("okItem");
                ok.innerHTML = '&#xe6fe;';
                li.appendChild(ok);
            }
            if (i.constructor === File) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    (new A.Views.FilesItem({
                        data: data,
                        html: e.target.result
                    }, el.id)).render();
                    A.My.renderDOM();
                    // return true;
                    if (resolve) resolve();
                };
                reader.readAsText( data );
            }
        };
        let el = gid( A.ID.list_of_files );
        if ( el && data ){
            switch (el.tagName){
                case 'UL':
                    if ( Array.isArray( data[key] )){
                        let arr = data[key], it;
                        for ( let i = 0; i < arr.length; ++i ){
                            it = arr[ i ];
                            addItem( it, el )
                        }
                    } else if ( data.constructor === File ){
                        addItem(data, el)
                    }
            }
        }
    };
    /**
     * remove parent item
     *
     * @param {HTMLElement} el
     * */
    Form.prototype.removeItem = function ( el ) {
        let parentId = arg.target.parentNode.id;
        if (parentId && My.VDom[ parentId ]){
            delete My.VDom[ parentId ];
        }
        el.parentNode.remove();
    };

// --------------------------------------------------------------------------------------------

    /**
     * Виртуальный ДОМ - основной элемент представлений
     * 1. компонент создается
     * 2. рендериться - создается его полный html
     * 3. вписывается ссылка на него в родительский компонент, если таковой есть
     * 4. определяется необходимость в рендеринге в DOM
     * 5. выполняется полный рендринг
     * наследование на прототипах: https://learn.javascript.ru/class-inheritance
     * */
    let My = function (){};
    /**
     * содержит список компонентов прошедщих предварительный рендеринг
     *
     * @return {object} My.VDom
     */
    My.VDom = {};
    /**
     * содержит список компонентов для текущего URL
     *
     * @return {object} My.Content
     */
    My.Content = {};
    /**
     * управляет renderStatus и обновляет, при необходимости, комп-ты в VDom
     *
     * @this {My.VDom}
     * @set this.statusRender
     */
    My.prototype.setRenderStatus = function(){
        let elemInVD = My.VDom[ this.prop.id ];
        if ( elemInVD ){
            if ( this.template !== elemInVD.template ){
                My.VDom[ this.prop.id ].template = this.template;
                My.VDom[ this.prop.id ].statusRender = 1;
            } else if ( elemInVD.forseRender === 1 ){
                My.VDom[ this.prop.id ].statusRender = 1;
            } else {
                My.VDom[ this.prop.id ].statusRender = 0;
            }
        } else {
            My.VDom[ this.prop.id ] = this;
            My.VDom[ this.prop.id ].statusRender = 1;
        }
    };
    /**
     * подстановка значений вместо плейсхолдеров в шаблоне компонента
     *
     * @this {My.VDom}
     * @set this.prop.template
     */
    My.prototype.setHtml = function(){
        let template = this.template;
        let replace = function($token, $value){
            return template.replace( new RegExp("{{"+$token+"}}", 'g'), $value )
        };
        if ( this.prop ){
            for (let name in this.prop ){
                let token, value;
                if ( this.prop.hasOwnProperty( name ) ){
                    if ( name === 'nested' ){
                        let obj = this.prop.nested;
                        for ( let nm in obj ){
                            token = 'nested.' + nm;
                            if (obj.hasOwnProperty(nm)){
                                value = obj[ nm ];
                            }
                            template = replace(token, value);
                        }
                    } else {
                        token = name;
                        if ( token === 'class' ) {
                            value = '"'+this.prop[ name ]+'"';
                        } else {
                            value = this.prop[ name ];
                        }
                        // console.log ( token + ' == ' + value );
                    }
                    template = replace( token, value );
                }
            }
        }
        My.Content[ this.prop.id ] = 1; // list active comp
        this.template = template;
    };
    /**
     * прописывает ссылки на вложенные компоненты в родительском компоненте
     *
     * @this {My.VDom}
     * @return this.VDom.nested
     * */
    My.prototype.setParentComp = function(){
        let parent = this.prop.parent || this.place;
        // debugger
        if ( My.VDom[ parent ] && !My.VDom[ parent ].prop.nested ) My.VDom[ parent ].prop.nested = {};
        if ( My.VDom[ parent ] && My.VDom[ parent ].prop.nested ) My.VDom[ parent ].prop.nested[ this.prop.id ] = this.prop.id;
    };
    /**
     * удаляет компоненты не требующиеся для данного URL из DOM и VDom
     *
     * @this {My.VDom}
     * @delete this[ id ]
     * @remove div
     * */
    My.prototype.removeComp = function(){
        // console.log ( JSON.stringify( My.Content ));
        let div;
        for (let id in My.VDom){
            if ( My.VDom.hasOwnProperty(id) && !My.Content[id]){
                delete My.VDom[ id ];
                div = gid( id );
                if ( div ) div.remove();
            }
        }
    };
    /**
     * очищает список не представленных комп-в
     *
     * @this {My}
     * @set this.Content
     * */
    My.prototype.clearCompList = function(){
        // console.log ( JSON.stringify( My.Content ));
        My.Content = {};
    };
    /**
     * если родитель получил statusRender = 1, то устанавливает тот же статус для вложенных компонентов
     *
     * @this {My}
     * @set this.statusRender = 1
     */
    My.prototype.setNestedRenderStatus = function(){
        for ( let id in My.VDom ){
            if ( My.VDom.hasOwnProperty( id ) && My.VDom[ id ].statusRender === 1 ){
                let comp = My.VDom[ id ];
                let nested = comp.prop.nested;
                if ( nested ){
                    for ( let name in nested ){
                        if ( nested.hasOwnProperty( name )){
                            My.VDom[nested[ name ]].statusRender = 1;
                        }
                    }
                }
            }
        }
    };
    /**
     * render in DOM
     *
     * @this {My}
     * @set this.outerHTML
     */
    My.renderDOM = function(){
        /**
         https://habrahabr.ru/sandbox/91185/ DOMparser
         https://learn.javascript.ru/range-textrange-selection
         https://developer.mozilla.org/ru/docs/Web/API/Range/createContextualFragment
         https://learn.javascript.ru/multi-insert
         способы вставки:
         append/prepend – вставка в конец/начало.
         before/after – вставка перед/после.
         replaceWith – замена.
         */

        this.prototype.setNestedRenderStatus();

        let Render = function ( comp ) {
            if ( !comp.place && !comp.prop.parent ){
                return console.error('empty place and parent property in ' + id + ', check this component');
            }
            if ( comp.place && comp.prop.parent ){
                return console.error('set place and parent property in ' + id + ', check this component');
            }
            let SR = function () {
                comp.statusRender = comp.isRemoved = 0;
            };
            let HTML = function (template) {
                let html;
                if (template) {
                    html = template;
                } else {
                    html = '';
                }
                return html;
            };
            let div, place, template;
            template = comp.template;
            if ( comp.statusRender === 1 && comp.place ){ // вставка корневого эл-та
                // console.log ( 'insert root elem' );
                place = gid( comp.place );
                div = gid( comp.prop.id );
                if ( !place ){
                    Render( My.VDom[ comp.place ]);
                }
                if ( !place ) place = gid( comp.place );
                if ( !div ) div = gid( comp.prop.id );
                if ( div ){
                    div.outerHTML = HTML( template );
                    SR();
                    return;
                } // https://developer.mozilla.org/ru/docs/Web/API/Range/createContextualFragment
                if ( place ){
                    place.insertAdjacentHTML( Where[comp.prop.where] || 'beforeEnd', HTML( template ));
                    SR();
                }
            } else if ( comp.statusRender === 1 && comp.prop.parent ){ // вставка подэлемента
                // console.log ( 'insert nested elem' );
                div = gid( comp.prop.id );
                if ( !div ){
                    Render( My.VDom[ comp.prop.parent ]);
                } else {
                    div.outerHTML = HTML( template );
                }
                SR();
            } else if (comp.UpState === 1) {
                for (let i in comp.state) {
                    if (comp.state.hasOwnProperty(i)) {
                        console.log('set state');
                        // console.log ( comp.state[ i ] );
                        gid(comp.prop.id).setAttribute(i, comp.state[i]);
                        comp.UpState = 0;
                        if (comp.state[i] === 'set') {
                        }
                    }
                }
            }
        };
        for (let id in My.VDom) {
            // debugger
            if (My.VDom.hasOwnProperty(id)) {
                let comp = My.VDom[id];
                Render( comp );
            }
        }
    };
// --------------------------------------------------------------------------------------------

    /**
     * модуль, обработчик исключений
     *
     * @param {object} arg
     * @return {object} arg
     * */
    let err = (function (){
        let run = function ( data )
        {
            try {
                // debugger
                console.log( data );
                console.log( data.eventFunc.toString() );
                if ( data.code === 10 ){
                    data.arg.step++;
                    return Mediator( data.arg );
                }
                if (data.event) console.warn(data.event);
                if (data.err) {
                    console.info(data.err.data);
                    console.warn(data.err['stack']);
                }
                if ( data.resp && data.resp.indexOf('xdebug-error') !== -1 ){
                    let err = document.createElement('div');
                    err.innerHTML = data.resp;
                    let cont = document.querySelector('.content');
                    cont.insertBefore(err, cont.children[1]);
                }
            } catch( e ) {
                console.warn( e );
                // console.warn(arg.err['stack']);
            }
        };
        return {
            run: run
        };
    })();

    function Xhr( arg, resolve ){
        // debugger
        if (dev) console.log ( 'Xhr' );
        new Promise((resolve, reject )=>{
            let xhr = new XMLHttpRequest();
            let data = new FormData(), i;
            for (i in arg.req.obj) {
                if (arg.req.obj.hasOwnProperty(i)) data.append(i, arg.req.obj[i]);
            }
            if (arg.file) {
                data.append( arg.file.name, arg.file );
            }
            arg.dataXHR = data;
            xhr.onerror = reject;
            // debugger
            let progress = function (e) {
                // console.log ( e );
                // debugger
                if (e['lengthComputable']) {
                    arg.progress = (e.loaded * 100) / e.total;
                    if (arg.onprogress instanceof Function) {
                        arg.onprogress.call(this, Math.round(arg.progress));
                    }
                }
            };
            xhr.open('POST', '/init.php');
            xhr['onreadystatechange'] = arg['loadend'];
            xhr.onload = function(){
                console.log ( this.response );
                // debugger
                if (typeof this.response === 'string'){
                    arg.resp = JSON.parse( this.response );
                }
                resolve( this.response );
            };
            xhr.upload.onprogress = progress;
            xhr.send( arg.dataXHR );
        })
            .then(()=>{
                arg.req.metod = null;
                arg.onload();
                resolve( arg );
            })
    }

    /**
     * fetch запрос
     * https://fetch.spec.whatwg.org/
     * https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    function Fetch( arg, resolve ) {
        // debugger

        fetch( arg.reqURL, {
            method: arg.req.metod,
            body: arg.fetchInit.body || null,
            credentials: arg.fetchInit.credentials || null
        })
            .then(function (response) {
                return response.json();
            })
            .then((data)=>{
                if (dev) console.log ( data );
                arg.req.metod = null;
                arg.resp = data;
                // можно определить действие внутри модуля или события
                // debugger
                if (arg.sm && typeof(arg.sm) === "function"){
                    arg.sm( arg );
                    arg.sm = false;
                }
                resolve( arg )
            })
            .catch(( err )=>{
                // debugger
                err.eventFunc = arg.event.f;
                A.err.run( err )
            });
    }

    /**
     * посредник м/у наблюдателем и модулями
     *
     * @param {object} arg
     * @return {object} arg
     * */
    function Mediator( arg ){
        arg.event = arg.req.act[ arg.step ];
        new Promise((resolve)=>{
                // debugger
                if (typeof ( arg.event.f ) === 'function') {
                    arg.event.f()( arg, resolve );
                }
            }
        )
            .then(
                () => {
                    if ( typeof ( arg.event.f ) === 'function' ){
                        let next = arg.req.act[ ++arg.step ];
                        if ( next ){
                            let action = next.f;
                            if ( action ){
                                arg.event = action;
                                return Mediator( arg );
                            }
                        }
                    }
                }
            )
            .catch((e)=>{
                // debugger
                e.eventFunc = arg.event.f;
                err.run( e )
            })
        // .catch(if (dev) console.log.bind(if (dev) console))
    }

    /**
     * модуль переключения класса при focus/blur/change/click
     *
     * @param {object} e
     * @return {state}
     * */
    let focus = (function (){
        let run = function ( e ){
            if (e.target === undefined || e === null || e.target === window) return;
            let eventType = e.type;
            let target = e.target;
            let tagType = target.type;
            let tagName = target.tagName;
            switch (eventType) {
                case 'focus':
                    if (tagName === 'A') {
                        target.classList.add('focus');
                    }
                    if (tagName === 'INPUT' || tagName === 'BUTTON') {
                        if (target.closest('label') !== null) {
                            target.closest('label').classList.add('focus');
                        } else {
                            target.classList.add('focus');
                        }
                    }
                    break;
                case 'blur':
                    if (tagName === 'A') {
                        target.classList.remove('focus');
                    }
                    if (tagName === 'INPUT' || tagName === 'BUTTON') {
                        if (target.closest('label') !== null) {
                            target.closest('label').classList.remove('focus');
                        } else {
                            target.classList.remove('focus');
                        }
                    }
                    break;
                case 'change':
                    if (tagType === 'checkbox') {
                        if (target.closest('label') !== null) {
                            if (target.checked) {
                                target.closest('label').classList.add('checked');
                            } else {
                                target.closest('label').classList.remove('checked');
                            }
                        }
                    }
                    if (tagType === 'radio') {
                        let radio_arr = document.getElementsByName(target.name);
                        for (let i = 0, len = radio_arr.length; i < len; i++) {
                            let item = radio_arr[i];
                            if (item.checked) {
                                if (item.closest('label') !== null) {
                                    item.closest('label').classList.add('selected');
                                }
                            } else {
                                if (item.closest('label') !== null) {
                                    item.closest('label').classList.remove('selected');
                                }
                            }
                        }
                    }
                    break;
                case 'click':
                    if (tagName === 'A') {
                        let el_arr = document.querySelectorAll('nav, menu');
                        for (let i = 0, len = el_arr.length; i < len; i++) {
                            let a_arr = el_arr[i].getElementsByTagName(tagName);
                            for (i = 0, len = a_arr.length; i < len; i++) {
                                let item = a_arr[i];
                                if (item === target) {
                                    item.classList.add('selected');
                                    item.classList.add(SEL.active);
                                } else {
                                    item.classList.remove('selected');
                                    item.classList.remove(SEL.active);
                                }
                            }
                        }
                    }
                    break;
            }
        };
        return {
            run: run
        }
    })();
    /**
     * фасад А, все действия пользователя проходят через данный модуль
     *
     * @param {object} event
     * @return {object} arg
     * */
    let facade = (function (){
        let run = function ( event ){
            try {
                let e = event, ac;
                if ( e === null ) return;
                if ( e === undefined ) return;
                if ( !arg.req ) arg.req = {};
                if ( e.type === 'submit' ) {
                    if (dev) console.log ( 'выход' );
                    return e.preventDefault();
                }
                if ( e.target && e.target.closest !== undefined ){
                    // console.log ( e.target );
                    // console.log ( 'enabled = ' + e.target.getAttribute('enabled') );
                    // console.log ( 'disabled = ' + e.target.getAttribute('disabled') );
                    ac = e.target.closest([ '.a' ]);
                    if ( ac === null || e.target.disabled ||
                        e.target.parentNode.disabled ||
                        e.target.getAttribute('disabled') === 'disabled') {
                        return;
                    }
                    // это если dataset отсутствует, нужно для обхода проблемы двойного
                    if ( ac.dataset.a === null || !ac.dataset.a ) return;
                    //if (dev) console.log ( event );
                }
                if ( typeof e === 'string' ){
                    if (dev) console.log ( e );
                    try {
                        arg.req.obj = JSON.parse( decodeURIComponent( e.substring( 1 )));
                    } catch (e) { // направим на главную страницу
                        arg.req.obj = {"select":"ob","mode":"page","data":{"id":"1"}};
                    }
                }
                else if ( e.type === 'input' ){} // событие для текстовых полей
                else if ( e.type === 'click' ){
                    // console.log ( e.target.tagName );
                    if ( e.target.tagName === 'A' ) {
                        // debugger
                        e.preventDefault();
                    }
                    if (e.target.tagName === 'INPUT' || e.target.tagName === 'TEXTAREA' ) return;
                    // здесь выйдем потому, что есть обработчик события change
                    if (e.target.tagName === 'IMG'){
                        e.preventDefault();
                    }
                    // здесь выйдем, т.к. если инпут вложен в label, то клик срабатывает дважды
                    if ( ac.tagName === 'LABEL' || ac.tagName === 'INPUT' ) return;
                    // предотвратим переходы по ссылкам, также не будут отмечатся чекбоксы и т.д.
                    if ( ac.dataset.a === 'test' ){
                        e.preventDefault();
                        if (dev) console.log ( arg );
                        return;
                    }
                }
                // соб для инпутов типа file
                else if ( e.type === 'change' ){
                    // console.log ( 'file-change' );
                } else {
                    return; // если ничего из перечисленного остановим обработку события
                }
                if ( ac && ac.dataset.a ){
                    arg.req.obj = JSON.parse( ac.dataset.a );
                    arg.target = ac;
                }
                let act = A.actions[ arg.req.obj.select + '_' + arg.req.obj.mode ];

                if ( act && act.length )
                {
                    arg.req.act = act;
                    arg.req.hash = JSON.stringify( arg.req.obj );
                    if ( !arg.event ) arg.event = {};
                    arg.step = 0;
                    arg.resp = { code:0 };
                    arg.cs.cln = { // состояние приложения
                        err: 0,
                        runReq: 0,
                        formValid: 0,
                        srv: 0
                    };
                    // debugger
                    Mediator( arg );
                }
            } catch (e) {
                arg.cs.cln.err = 1;
                arg.err = e;
                err.run( arg );
            }
        };
        return {
            run: run
        }
    })();
    /**
     * план-список вызовов
     *
     * @param {string} a
     * @return {array}
     * */

    actions = {
        App_set: [ // на старте приложения
            {f: () => getScript, d: '/scripts/views.js'},
            {f: () => getScript, d: '/scripts/events.js'},
            {f: () => getScript, d: '/scripts/actions.js'},
            {f: () => RunApp }
            // {f: () => A.Ets.pause }
        ]
    };
    A.actions = actions;

    /**
     * событие, импорт JS скрипта
     *
     * @param {object} arg // arg.event.d - местоположение скрипта
     * @param resolve
     * @return {function} JS()
     * */
    function getScript ( arg, resolve ){
        // debugger
        if (dev) console.log ( 'getScript' );
        let s = document.querySelector('script[src="' + arg['event'].d +'"]');
        if ( !s ){
            let script = document.createElement('script');
            script.src = arg['event'].d;
            script.type = arg['event'].t || A.typeScript;
            document.body.appendChild( script );
            script.onload = resolve;
        } else {
            resolve( arg )
        }
    }

    function RunApp() {
        facade.run( window.location.hash );
    }

    window.addEventListener('submit', facade.run);
    window.addEventListener('change', facade.run); // для текстовых полей
    window.addEventListener('input', facade.run);
    window.addEventListener('click', facade.run, false); // умолчание http://javascript.ru/tutorial/events/intro#perehvat-sobytiy-capturing
    window.addEventListener('click', focus.run, false);
    window.addEventListener('focus', focus.run, true);
    window.addEventListener('blur', focus.run, true);

    window.addEventListener('popstate', function (e){
        // if (dev) console.warn ( 'popstate' );
        // if (dev) console.log ( this );
        // if (dev) console.log ( e );
    });
    window.addEventListener('hashchange', function (){
        if (dev) console.log ( 'hashchange' );
        // console.log ( arg.target );
        if ( A.hashIsSet ){
            A.hashIsSet = false;
        } else {
            if ( !arg.target ) {
                if (dev) console.warn( 'переход' );
                facade.run( location.hash );
            }
        }
        My.prototype.clearCompList();
    });
    facade.run("#{\"select\":\"App\",\"mode\":\"set\"}");

    A.Color = Color;
    A.My = My;
    A.SEL = SEL;
    A.exp = exp;
    A.arg = arg;
    A.checkEl = checkEl;
    A.Mediator = Mediator;
    A.err = err;

    A.gid = gid;
    A.facade = facade;
    A.Common = Common;
    A.Where = Where;
    A.Form = Form;
    A.Fetch = Fetch;
    A.Xhr = Xhr;
    A.getScript = getScript;
    A.Views = {}; // Представления
    A.ID = {}; // ИД представлений
    A.Ets = {}; // События

    window.A = A;

})();
