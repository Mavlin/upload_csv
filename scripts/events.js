"use strict";
//console.time("All Benchmarks");
(function () {
    /**
     * содержит ID элементов
     *
     * @return {string}
     */
    let ID = Object.create( null );
    ID = {
        mainPage: 'mainPage',
        content: 'content',
        lft_btn: 'lft_btn',
        rght_btn: 'rght_btn',
        sidebar1: 'sidebar1',
        sidebar2: 'sidebar2',
        ob: 'ob',
        ob_list: 'ob_list',
        ob_pagination: 'ob_pagination',
        ObAdd: 'ObAdd',
        fill: 'fill',
        response: 'response',
        case: 'case',
        newForm: 'newForm',
        sendButton: 'sendButton',
        place: 'place',
        list_of_files: 'list_of_files',
        taskOneID: 'taskOneID',
        page: 'page',
        echo: 'echo',
        obCase: 'obCase',
        pgnShowHideBtn: 'pgnShowHideBtn',
        obPaginationCaseId: 'obPaginationCaseId'
    };

    const
        ROW_COUNT = 7;

    let
        Ets = {},
        Form = A.Form,
        dev = A.dev,
        ExpangeCanvas = A.ExpangeCanvas,
        facade = A.facade,
        My = A.My,
        Views = A.Views,
        Common = A.Common;

    /**
     * вставка элементов на страницу
     *
     * @param {object} params
     * @return {HTMLElement}
     * */
    let ins = (function (){
        let run = function (params){
            let el = document.createElement(params.tag || 'p'), cls;
            if (params.id) {
                el.id = params.id;
            }
            if (typeof params.html === 'function') {
                el.innerHTML = params.html();
            } else {
                let html = params.html;
                if (params['plch']) {
                    for (let name in params['plch']) {
                        if ( params['plch'].hasOwnProperty(name) ){
                            params.html = html.replace( new RegExp("{"+name+"}", 'g'), params['plch'][name] );
                        }
                    }
                }
                el.innerHTML = params.html || null;
            }
            if (params.name) {
                el.name = params.name;
            }
            if (params.value) {
                el.value = params.value;
            }
            if (params.clas) {
                if (typeof params.clas === 'function') {
                    cls = params.clas();
                } else {
                    cls = params.clas;
                }
                if (cls !== undefined) {
                    let arr = cls.split(' ');
                    for (let i = 0; i<arr.length; ++i){
                        el.classList.add( arr[i] );
                    }
                }
            }
            if (params.attr) {
                for (let attr in params.attr) {
                    if (params.attr.hasOwnProperty(attr)) {
                        el.setAttribute(attr, params.attr[attr]);
                    }
                }
            }
            for (let arg in params){
                if (params.hasOwnProperty(arg) && params[arg] === true){
                    el[arg] = true;
                }
            }

            if (params.style) {
                for (let style in params.style) {
                    if (params.style.hasOwnProperty(style)) {
                        if (typeof params.style[style] === 'function') {
                            el.style[style] = params.style[style]();
                        } else {
                            el.style[style] = params.style[style];
                        }
                    }
                }
            }
            let parent = params.parent;
            if (params['orderInParent'] !== undefined) {
                params.result = parent.insertBefore(el, params.parent.children[params['orderInParent']]);
                return;
            }
            return parent.insertBefore(el, null);
        };
        return {
            run: run
        }
    })();

    /**
     * событие, импортируем файл, например HTML
     *
     * @param {string} arg.event.d - местоположение файла
     * @return {string} response
     * */
    Ets.getFile = function (arg, resolve){
        if (dev) console.log ( 'getFile' );
        let xhr = new XMLHttpRequest();
        xhr.open( 'GET', arg.event.d );
        xhr.onloadend = function () {
            arg.resp.data = this.response;
            resolve( arg );
        };
        xhr.send();
    };
    /**
     * подготовка запроса данных для вывода списка объявлений
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @return {object} arg
     * */
    Ets.obListReq = function (arg, resolve){
        if (dev) console.log ( 'obListReq' );
        arg.req.obj.select = 'ob';
        arg.req.obj.mode = 'list';
        arg.cs.cln.runReq = 1;
        resolve( arg );
    };
    /**
     * подготовка запроса данных для вывода главной страницы
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @return {object} arg
     * */
    Ets.mainPageReq = function (arg, resolve){
        if (dev) console.log ( 'mainPageReq' );
        arg.req.obj.select = 'main';
        arg.req.obj.mode = 'view';
        arg.cs.cln.runReq = 1;
        resolve( arg );
    };
    /**
     * подготовка основного запроса - нерекомендован - неочевиден
     *
     * @param {object} arg - глобальный массив данных в области видимости модуля А
     * @return {object} arg
     * */
    Ets.setReqFromURL = function ( arg, resolve ){
        if (dev) console.log ( 'setReqFromURL' );
        if ( typeof( arg.req.hash ) === 'string' ){
            arg.req.obj = JSON.parse( arg.req.hash );
            arg.cs.cln.runReq = 1;
            // console.log ( arg );
        }
        resolve( arg );
    };
    /**
     * вывод главной страницы
     *
     * @param {object} arg
     * @return {object} arg
     * */
    Ets.mainPage = function (arg, resolve){
        if (dev) console.log ( 'mainPage' );
        A.checkEl( ID.content );
        let data = arg.resp.data;
        new Views.Place({
            id: ID.mainPage,
            html: data
        }, ID.content).render();
        document.title = 'Главная страница';
        My.renderDOM();
        if (dev) console.log ( My.VDom );
        resolve( arg );
    };
    /**
     * пауза, иногда для отладки
     *
     * @param {object} arg
     * @return {object} arg
     * */
    Ets.pause = function (arg, resolve){
        if (dev) console.log ( 'pause' );
        if (dev) console.log ( My.VDom );
        debugger;
        resolve( arg );
    };
    /**
     * renderDOM
     *
     * */
    Ets.renderDOM = function ( arg, resolve ){
        if ( dev ){
            console.log ( 'renderDOM' );
            console.time('start VDom');
        }
        My.renderDOM();
        if ( dev ){
            console.log ( My.VDom );
            console.timeEnd('start VDom');
        }
        resolve( arg );
    };
    // /**
    //  * обновление location.hash, здесь же обнуляется список компонентов VDom
    //  * создает событие hashchange
    //  *
    //  * @param {object} arg
    //  * @return {object} arg
    //  * */
    Ets.setHash = function (arg, resolve){
        if (dev) console.log ( 'setHash' );
        if ( arg.resp.code === 0 ) {
            if ( decodeURIComponent( location.hash ) !== '#' + arg.req.hash && !A.hashIsSet ){
                A.hashIsSet = true;
                location.hash = arg.req.hash;
            }
        }
        resolve( arg );
    };
    /**
     * удаление лишних компонентов для текущего URL
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.removeComp = function (arg, resolve){
        if (dev) console.log ( 'removeComp' );
        My.prototype.removeComp();
        arg.target = null; // для события hashchange
        resolve( arg );
    };
    /**
     * очищает список компонентов VDom
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.clearListComp = function (arg, resolve){
        if (dev) console.log ( 'clearListComp' );
        My.prototype.clearCompList();
        resolve( arg );
    };
    /**
     * перезапросим данные после входа
     *
     * @param {object} arg
     * @return {object} arg
     * */
    Ets.reloadReq = function ( arg ){
        if ( arg.resp.code === 0 ){
            if (dev) console.log('reloadReq');
            facade.run( arg.event.d );
        }
    };
    /**
     * Fetch request
     *
     * @param {object} arg
     * @param resolve
     * @param reject
     * @return {object} arg
     * */
    Ets.fetch = function (arg, resolve ){
        if ( arg.cs.cln.runReq ){
            if (dev) console.log( 'fetch' );
            arg.reqURL = '/init.php';
            arg.reqData = JSON.stringify( arg.req.obj );
            arg.fetchInit = {};
            arg.fetchInit.credentials = 'include'; // отослать куки - пока требуется только в отладке

            if ( arg.req.metod === 'get' || !arg.req.metod ) {
                arg.req.metod = 'get';
                arg.reqURL += '?req=' + arg.reqData;
            }
            else if ( arg.req.metod === 'post' ){
                let data = new FormData(), i;
                for (i in arg.req.obj) {
                    if (arg.req.obj.hasOwnProperty(i)) data.append(i, arg.req.obj[i]);
                }
                arg.fetchInit.body = data;
            }
            A.Fetch( arg, resolve );
        } else {
            resolve( arg );
        }
    };
    /**
     * управляем видимостью блока
     *
     * */
    Ets.changeVisible = function ( arg, resolve ){
        if (dev) console.log ( 'changeVisible' );
        let el = A.gid( arg.event.d ), tf = 'cubic-bezier(1,.2,.9,1.7)';
        if ( el && el.dataset.visible === 'show' ){
            slide.up( arg.event.d, 250, tf );
            el.dataset.visible = arg.target.dataset.visible = 'hide';
        } else if ( el && el.dataset.visible === 'hide' ){
            slide.down( arg.event.d, null, 250, tf );
            el.dataset.visible = arg.target.dataset.visible = 'show';
        }

        resolve( arg );
    };
    /**
     * формирование списка об-ий
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.renderPagination = function( arg, resolve ){
        if (dev) {
            console.log ( 'renderPagination' );
            // console.time('start VDom');
        }
        let data = arg.resp.data, where, i=0;
        A.checkEl( ID.content );
        new Views.Place({
            id: ID.pgnShowHideBtn,
            html: '<i></i><i></i><i></i>',
            class: 'pgnShowHideBtn a',
            visible: 'hide',
            link: '{"select":"common","mode":"changeVisiblePgn"}'
        }, ID.content).render();

        arg.groups = Math.ceil( data[ 'ob_count' ]/data[ 'ob_in_group' ]);
        let obPaginationCase = new Views.Place({
            id: ID.obPaginationCaseId,
            class: 'pagination',
            mark: data['ob_count'],
            visible: 'hide',
            hidden: 'hidden'
        }, ID.content);
        obPaginationCase.render();

        if ( arg.req.obj && arg.req.obj.data ){
            where = arg.req.obj.data;
        }
        // debugger
        while (i < arg.groups){
            new Views.PaginationItem({
                id: ID.ob + i,
                html: i+1,
                active: function () {
                    return (i+1 === +where.id) ? 'active' : ''
                }()
            }, ID.obPaginationCaseId).render();
            if (( i+1 ) % ROW_COUNT === 0 ){
                // debugger
                new Views.Place({
                    id: 'fill' + i,
                    class: 'fill'
                }, ID.obPaginationCaseId).render()
            }
            i++;
        }
        resolve( arg );
    };
    /**
     * отображение боковых кнопок
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.renderSideBtn = function(arg, resolve ){
        if (dev) {
            console.log ( 'renderSideBtn' );
        }
        let where;
        if ( arg.req.obj && arg.req.obj.data ){
            where = arg.req.obj.data;
        }
        new Views.SideBtn({
            id: ID.lft_btn,
            html: '&#xf100;',
            data: arg.groups,
            direction: 'back',
            state: function () {
                return ( where.id < 2 ) ? 'disabled' : 'enabled'
            }()
        }, ID.sidebar1).render();
        new Views.SideBtn({
            id: ID.rght_btn,
            html: '&#xf101;',
            data: arg.groups,
            direction: 'next',
            state: function () {
                return ( where.id >= arg.groups ) ? 'disabled' : 'enabled'
            }()
        }, ID.sidebar2).render();

        resolve( arg );
    };
    /**
     * вывод страницы объявлений
     *
     * @param {object} arg
     * @param resolve
     * @return {object} arg
     * */
    Ets.renderPage = function(arg, resolve ){
        if (dev) {
            console.log ( 'renderPage' );
        }
        let data = arg.resp.data;
        if (dev) console.log ( data );
        A.checkEl( ID.content );
        let obs = data['ob_data'], page = 1;
        if (obs.length){
            new Views.Place({
                id: ID.obCase,
                class: 'obCase'
            }, ID.content).render();
            obs.some(function ( el, i ){
                new Views.Page({
                    id: ID.ob + i,
                    data: el
                }, ID.obCase).render();
            });
        }
        // debugger
        if (!obs.length){
            new Views.Place({
                id: 'emptyPage',
                html: 'эта страница не содержит объявлений',
                class: 'emptyPage'
            }, ID.content).render()
        }
        // debugger
        if (arg.req.obj.data){
            page = arg.req.obj.data.id;
        }
        document.title = 'Страница #'+page;
        resolve( arg );
    };
    /**
     * добавляет форму для нового объявления
     *
     * @param {object} arg
     * @return {HTMLElement} in DOM
     *
     * */
    Ets.obAdd = function ( arg, resolve ){
        if (dev) console.log ( 'obAdd' );
        document.title = 'добавить объявления';
        let obNew = new Views.ObAddForm({
            html: arg.resp.data,
        }, ID.content);
        obNew.render();
        My.renderDOM();
        if (dev) console.log ( My.VDom );
        // debugger
        arg.form = new Form( obNew.prop.id );
        resolve( arg );
    };
    /**
     * покажем первое задание
     *
     * @param {object} arg
     * @return {HTMLElement} in DOM
     *
     * */
    Ets.taskOne = function ( arg, resolve ){
        if (dev) console.log ( 'taskOne' );
        document.title = 'первое задание';
        new Views.Place({
            id: ID.taskOneID,
            html: arg.resp.data,
        }, ID.content).render();
        My.renderDOM();
        if (dev) console.log ( My.VDom );
        // debugger
        resolve( arg );
    };
    /**
     * отображение на форме выбранных ф-в, события отображения асинхронны, но выполняются синхронно, чтобы ограничить
     * кол-во выбираемых файлов
     * @param {object} arg
     *
     * @param resolve
     * @param i
     * */
    Ets.showImg = function ( arg, resolve = null, i = 0 ){
        if (dev) console.log('showImg');
        if ( arg.target ){
            // debugger
            Ets.selectForm(arg);
        }
        let ul = arg.form.form.querySelector('ul.file'),
            ulLength = arg.form.form.querySelectorAll('ul.file li').length;
        if ( ulLength < ul.dataset.max ){
            new Promise((resolve)=>{
                arg.form.setFileData(arg.form.fileInput.files[i], 'list_of_files', resolve);
            }).then(function (){
                Ets.showImg(arg, resolve, ++i );
            }).catch(function (e) {
                console.log ( e );
            })
        } else {
            if (dev) console.log("лишние файлы отсеяны");
            // обнулим выбор файлов, иначе повторно тот же файл не выбрать
            arg.target.type = '';
            arg.target.type = 'file';
            arg.target = null;
        }
        resolve( arg );
    };

    /**
     * удаление родительского объекта - ???
     *
     * @param {object} arg
     * */
    Ets.removeItem = function ( arg, resolve ){
        if (dev) console.log('removeItem');
        Form.prototype.removeItem( arg.target );
        resolve( arg );
    };

    /**
     * раскрытие блока
     * @param {object} arg
     * */
    Ets.slideDown = function ( arg, resolve ){
        if (dev) console.log('slideDown');
        slide.down( arg.target, arg.resp.data, 350);
        resolve(arg);
    };
    /**
     * вывод страницы
     * @param {object} arg
     * */
    Ets.getPage = function ( arg, resolve ){
        if (dev) {
            console.log('getPage');
            console.log ( arg.req.obj );
        }
        let
            data,
            currUrlHash = location.hash, currUrl;
        if (currUrlHash){
            try {
                currUrl = JSON.parse( decodeURIComponent( currUrlHash.substring( 1 )));
            } catch (e) { // направим на главную страницу
                arg.req.obj = {"select":"ob","mode":"page","data":{"id":"1"}};
            }
        }
        if ( arg.req.obj && arg.req.obj.data ){
            data = arg.req.obj.data;
        }
        if ( data ){
            if (data.id === 'back'){
                data.id = --currUrl.data.id;
            } else if (data.id === 'next'){
                data.id = ++currUrl.data.id;
            }
            arg.req.hash = JSON.stringify( arg.req.obj )
        }
        resolve(arg);
    };
    /**
     * обработка ответов сервера
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.showResp = function ( arg, resolve = null ){

        if (dev) console.log('showResp; arg.resp.code = ' + arg.resp.code);
        let resSel = 'response', r, res;
        if ( arg.form ) {
            r = arg.form.form.querySelector('.' + resSel);
            if (r) r.remove();
            if ( arg.target ){
                // debugger
                res = ins.run({
                    tag: 'div',
                    hidden: true,
                    clas: resSel,
                    html: arg.resp.message || arg.resp.data,
                    parent: arg.target
                });
            }
        }
        function down() {
            slide.down( res );
        }
        function up() {
            setTimeout(function () {
                slide.up( res );
            }, 3000);
        }
        switch (arg.resp.code) {
            case 0:
                break;
            case 2: // size field over the limits
                // debugger
                if (dev) console.log ( arg.resp.data || arg.resp.message );
                new Views.ExceptionsItem({
                    id: ID.echo + '_' + arg.resp.data.fileName,
                    data: arg.resp.data,
                    where: 3,
                }, ID.echo).render();
                My.renderDOM();
                if (dev) console.log ( My.VDom );
                // return;
                arg.resp.code = 0;
                break;
            default:
                if (dev) console.log ( arg.resp.data || arg.resp.message );
                down( res );
                up( res );
                let inputs = arg.form.inputs;
                if ( inputs ) inputs[0].focus();
                arg.resp.code = 0;
                return;
        }
        if (resolve) resolve( arg );
    };
    /**
     * показать скрытые элементы
     *
     * @param {object} arg
     *
     * */
    Ets.showHideElem = function ( arg, resolve ){
        if (dev) console.log('showHideElem');
        if ( arg.resp.code === 0 ){
            arg.form.showAfterSaveElem();
            arg.form.showAdmElem();
            // debugger
            if (arg.event.d && arg.form_data[arg.event.d]['can_edit']) {
                arg.form.showOwnerElem();
            }
        }
        resolve( arg );
    };
    /**
     * событие отправки файлов
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.sendCsv = function (arg, resolve ){
        if (dev) console.log('sendCsv');
        let form = new Form(arg.target.closest('form'));
        let flist = form.files;
        if ( flist.length ){ //
            let li = form.files[0];
            let canvas = li.querySelector('canvas');
            let ok = document.createElement('a');
            arg.onprogress = function (value) { // позднее можно выделить в отд. модуль данное событие
                let x = 100;
                let y = 100;
                let color = '#8e600a';
                if (value < 100) {
                    let draw = new ExpangeCanvas(canvas, x, y);
                    // console.log ( value );
                    //draw.SetBgColor(bgColor);
                    draw.Circle(x / 2, y / 2, x * 0.4, 2, color, null, null, 100);
                    draw.Circle(x / 2, y / 2, x * 0.4 - 8, 10, color, null, null, value);
                    draw.Text(value + '%', x / 2, y / 2, 'italic bold 22px sans-serif', color);
                }
            };
            arg.onload = function (e) {
                // debugger
                // console.log ( arg.k + ' >>> ' + form.files.length );
                ok.classList.add("okItem");
                ok.innerHTML = '&#xe6fe;';
                (canvas.parentNode).replaceChild(ok, canvas);
                li.classList.add(A.SEL.sendToComplete);
                li.classList.remove(A.SEL.readyToSend);

                delete arg.onload; // чтобы не зациклить событие
                delete arg.onprogress;
                delete arg.file; // иначе при очередном сохранении файл будет отправлен вместе с основным запросом
// отправляется если есть элемент списка с классом readyToSend
                Ets.showResp( arg );
            };
            arg.req.obj.select = form.name;
            arg.req.obj.mode = 'save';
            let data = li.querySelector('textarea').innerHTML,
                dataCsv = Papa.parse(data,{
                    delimiter: ";",	// auto-detect
                    skipEmptyLines: true,
                    newline: "",	// auto-detect
                });
            dataCsv.fileName = li.id;
            arg.req.obj.data = JSON.stringify( dataCsv );
            arg.cs.cln.runReq = 1;
            // чтобы избежать зацикливания по каким-либо причинам ограничим кол-во отправок
            // debugger
            if ( arg.sendedFiles > 5 ){
                throw new UserException( 10, 'много слишком файлов пытаешься отослать ты((', arg );
            }
            A.Xhr( arg, resolve ); //
            arg.step--;
            return;
        }
        arg.resp.code = 0;
        arg.sendedFiles = 0;
        resolve( arg );
    };

    function UserException( code, message, arg ){
        this.code = code;
        this.message = message;
        this.arg = arg;
        this.name = "Исключение, определенное пользователем";
    }
    /**
     * отключить кнопку отправки
     *
     * @param {object} arg
     *
     * */
    Ets.disableSendButt = function ( arg, resolve ){
        if ( arg.resp.code === 0 ) {
            if (dev) console.log('disableSendButt');
            if (arg.form) {
                arg.form.submitButtonState().disabled();
            }
        } else {
            if (arg.form) {
                arg.form.submitButtonState().enabled();
            }
        }
        resolve( arg );
    };
    /**
     * отключить форму
     *
     * @param {object} arg
     *
     * */
    Ets.disableForm = function ( arg, resolve ){
        if (dev) console.log('disableForm');
        if (!arg.resp.code) {
            if (arg.sortable) arg.sortable.options.disabled = 1;
            arg.form.state(1);
            resolve( arg );
        } else {
            resolve( arg );
        }
    };

    /**
     * включить форму
     *
     * @param {object} arg
     *
     * @param resolve
     * */
    Ets.enableForm = function ( arg, resolve ){
        if (dev) console.log('enableForm');
        arg.form.state( 0 );
        if ( arg.sortable ) arg.sortable.options.disabled = 0;
        resolve( arg );
    };
    /**
     * включить кнопку отправки
     *
     * @param {object} arg
     *
     * */
    Ets.enableSendButt = function ( arg, resolve ){
        if (dev) console.log('enableSendButt');
        if ( arg.form ){
            arg.form.submitButtonState().enabled();
        }
        resolve( arg );
    };
    /**
     * отправить данные
     *
     * @param {object} arg
     *
     * */
    Ets.sendData = function ( arg, resolve ){
        if (dev) console.log('sendData');
        let form = arg.form;
        let data = form.getData();
        let frm = arg.form.form;
        arg.data = data;
        arg.req.metod = 'post';
        let req = JSON.parse( arg.req.hash );
        arg.req.obj.select = req.select;
        arg.req.obj.mode = frm.dataset.mode || req.mode;
        arg.req.obj.data = JSON.stringify( data );
        arg.cs.cln.runReq = 1;
        arg.cs.cln.show = frm.dataset.show;
        resolve( arg );
    };
    /**
     * показать прелоадер
     *
     * @param {object} arg
     * */
    Ets.showLoader = function ( arg, resolve ){
        if (dev) console.log('showLoader');
        Common.prototype.loader(arg.form.submitButt).show();
        resolve( arg );
    };
    /**
     * удалить прелоадер
     *
     * @param {object} arg
     * */
    Ets.removeLoader = function ( arg, resolve ){
        if (dev) console.log('removeLoader');
        if ( arg.form && arg.form.submitButt ){
            Common.prototype.loader( arg.form.submitButt ).remove();
        }
        resolve( arg );
    };
    /**
     * выберем активную форму
     *
     * @param {object} arg
     * */
    Ets.selectForm = function ( arg, resolve = null ){
        if (dev) console.log('selectForm');
        arg.form = new Form( arg.event.d || arg.target.closest('form') || arg.target.parentNode );
        if (resolve) resolve( arg );
    };

    /**
     * slide.up
     *
     * @param {string || HTMLElement} elem
     * @param {number} duration
     * @param {string} timing_function
     *
     * slide.down
     *
     * @param {string || HTMLElement} elem
     * @param {html} content
     * @param {number} duration
     * @param {string} timing_function
     *
     * slide.setHeight - если нужно плавно изменить высоту видимого блока
     *
     * @param {string || HTMLElement} elem
     * @param {html} content
     * @param {number} duration
     * @param {string} timing_function
     * */
    let slide = function slide(){
        let far, style;
        let el = function(elem, duration, timing_function){
            if ( typeof elem === 'string' ){
                far = document.getElementById( elem );
            } else if ( typeof elem === 'object') {
                far = elem;
            }
                // console.log ( far.style.display );
            far.style.transitionProperty = 'height, padding';
            far.style.transitionDuration = duration + 'ms';
            far.style.transitionTimingFunction = timing_function || 'ease-in-out'; //'cubic-bezier(.5, -1, .5, 2)';
            style = getComputedStyle(far);
        };
        let up = function (elem, duration, timing_function){
            duration = duration || 500;
            el( elem, duration, timing_function );
            // if (dev) console.log ( far.hidden );
            if (dev) {
                // console.log ( far );
                // console.log ( style.display );
                // console.log ( parseFloat( style.display ));
            }
            far.style.height = parseFloat(style.height)+'px';
            far.style.overflow = 'hidden';
            far.style.boxSizing = style.boxSizing;
            far.style.height = 0;
            far.style.padding = 0;
            tm( far, duration );
        };
        let tm = function(far, duration){
            setTimeout(function () {
                far.hidden = 'hidden';
                far.style.padding = '';
            }, duration)
        };
        let down = function (elem, content, duration, timing_function){
            duration = duration || 500;
            el( elem, duration, timing_function  );
            setHeight( content );
            setTimeout( function(){
                far.style.height = 'auto';
                far.style.width = '';
                far.style.display = 'flex'
            }, duration );
        };
        let setHeight = function ( content ){
            /**
             * метод установки высоты блока, отдельно исп-ся тогда когда height-transition установлено в css
             * и высота в css = 0
             */
            let bw = parseFloat(style.borderWidth);
            let bs = style.boxSizing;
            let pdT = parseFloat(style.paddingTop);
            let pdB = parseFloat(style.paddingBottom);
            let pdL = parseFloat(style.paddingLeft);
            let pdR = parseFloat(style.paddingRight);
            far.style.height = parseFloat(style.height)+'px';
            if ( content ) far.innerHTML = content;
            let cloneFar = far.cloneNode(true);
            // console.log ( style.display );
            if (style.display === 'none'){
                cloneFar.style.display = 'flex';
            } else {
                cloneFar.style.display = style.display;
            }
            // cloneFar.style.display = 'inline';
            cloneFar.style.position = 'absolute';
            far.style.height = 0;
            far.style.padding = 0;
            far.style.overflow = 'hidden';
            far.hidden = false;
            cloneFar.style.width = far.offsetWidth+'px';
            cloneFar.style.boxSizing = bs;
            cloneFar.style.height = 'auto';
            far.parentNode.appendChild(cloneFar);
            if (bs === 'border-box'){
                far.style.paddingTop = pdT + 'px';
                far.style.paddingBottom = pdB + 'px';
                far.style.paddingLeft = pdL + 'px';
                far.style.paddingRight = pdR + 'px';
                far.style.height = cloneFar.offsetHeight+'px';
            }
            if (bs === 'content-box'){
                far.style.position = 'static';
                far.style.paddingTop = pdT + 'px';
                far.style.paddingBottom = pdB + 'px';
                far.style.height = cloneFar.offsetHeight-(pdT+pdB)-bw*2+'px';
                far.style.width = cloneFar.offsetWidth-(pdL+pdR)-bw*2+'px';
            }
            cloneFar.remove();
        };
        return {
            down: down,
            up: up,
            setHeight: setHeight
        };
    }();

    function slider(e) {
        let target = e.target, clas = 'active';
        if ( !target.classList.contains('acc_header')) return;
        let el = target.nextSibling;
        if ( el.hidden ){
            slide.down( el );
            target.classList.add( clas )
        } else {
            slide.up( el );
            target.classList.remove( clas )
        }
    }

    A.slide = slide;
    A.ID = ID;
    A.Ets = Ets;

})();

