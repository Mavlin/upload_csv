<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta name="description" content="Сайт объявлений">
    <meta name="keywords" content="Сайт объявлений">

    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
    <link rel="stylesheet" href="/css/style.css">
</head>

<?php include("blocks/header.php") ?>

<main>
    <div id="sidebar1" class="sdb sidebar1"></div>
    <div id="content" class="content"></div>
    <div id="sidebar2" class="sdb sidebar2"></div>
</main>

<?php include("blocks/footer.php");?>

</html>