<?php
namespace Models;

class Ob extends Model{

    public function __construct($request = null){
        parent::__construct($request);
    }

    // парсит данные для вставки
    public function parseData( $body, $contacts ){
        return $this->connect->parse("(NULL,NOW(),?s,?s)", $body, $contacts);
    }

    // вставка
    public function addOb( $ins ){
        $instr = implode(",", $ins);
        $result = false;
        if (!empty($instr)){
            $result = $this->connect->query("INSERT INTO tbl_ob_data VALUES ?p", $instr);
        }
        return $result;
    }

    // получим список объявлений
    public function getList( $countOb ){
        $sql = "select * from tbl_ob_data order by ob_id desc limit ?i";
        return $this->connect->getAll( $sql, $countOb );
    }

    // получим страницу объявлений
    public function getPage($idOb, $countOb){
        $sql = "select ob_id as id, ob_date as date, ob_body as body, ob_contacts as contacts from tbl_ob_data 
where ob_id<=?i order by id desc limit ?i";
        return $this->connect->getAll( $sql, $idOb, $countOb );
    }

    // получим наименьший id об-я (необходимо для решения проблемы удаления всех записей и запроса первой страницы -
    // запрос ничего не выдаст
    public function getFirstID(){
        $sql = "select ob_id from tbl_ob_data limit 1";
        return $this->connect->getOne($sql);
    }

    // получим кол-во объявлений
    public function getCountOb(){
        $sql = "select cnt from tbl_count";
        return $this->connect->getOne($sql);
    }

    // полная очистка таблицы
    public function purgeOb(){
        $sql = "truncate table tbl_ob_data";
        return $this->connect->query( $sql );
    }

    // обновим кеш с кол-вом записей в основной таблице
    public function updCount(){
        return $this->connect->query("insert LOW_PRIORITY into tbl_count set cnt=(select count(ob_id) from tbl_ob_data)
 ON DUPLICATE KEY UPDATE cnt=(select count(ob_id) from tbl_ob_data)");
    }

}

