<?php
namespace Models;
class Model
{
    public $config;
    public $connect;

    public function __construct( $db_cnf, $request = null ){
        $this->config = $db_cnf;
        $this->connect = $this->connect();
    }

    private function connect(){
        return new SafeMySQL( $this->config );
    }
// валидация данных в запросе
    public function getLimits( $formName ){
        $sql = "SELECT tfl_name as name, tfl_regexp as regex, tfl_min_len as min, tfl_max_len as max 
FROM tbl_fields_limit where tfl_form =?s";
        return $this->connect->getAll($sql, $formName);
    }

// ответ на запрос ограничений на поля, в данном случае исп-ся на клиенте только для отображения в подсказке
    public function getAllLimits(){
        $sql = "SELECT tfl_name as name, tfl_form as form, tfl_min_len as min, tfl_max_len as max,
tfl_regexp as regex, tfl_caption as caption FROM tbl_fields_limit";
        return $this->connect->getAll( $sql );
    }

    /**
     * проверка наличия колонки в таблице
     *
     * @param $db string
     * @param $tbl string
     * @param $fld string
     * @return bool
     *
     * */
    public function isColumnExist( $db, $tbl, $fld ){
        $sql = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema=?s and 
TABLE_NAME=?s and Column_Name=?s";
        return $this->connect->getOne($sql, $db, $tbl, $fld);
    }
}
