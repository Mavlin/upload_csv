<?php
namespace Controllers;
require_once $_SERVER['DOCUMENT_ROOT'].'/SplClassLoader.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/handler.php';
require_once '../Controllers/ob.php';
$ob = new OB(null );
?>
<form id={{id}} class="add form_1 ob_new" data-name="ob">
    <p class="title">добавить объявления</p>
    <div class="file_case">
        <label class="file_btn" tabindex="1">
                <span class="main">выберите файл(ы)</span>
                <span class="info">до 7 шт.</span>
                <span class="info">только .csv</span>
                <span class="info">текст и контакты ограничены количеством символов <?=$ob::BODY_MAX_LENGTH?>
                    и <?=$ob::CONTACTS_MAX_LENGTH?> соответственно</span>
            <input class="a" type="file" data-a='{"select":"common","mode":"file_input"}' multiple accept=".csv">
        </label>
        <ul id="list_of_files" class="file list textToSend" data-name="list_of_files" data-max=7></ul>
    </div>
    <div class="txt_case">
        <div id='echo' class="echo id" data-name="id"></div>
            <button class="btn a save" type="submit" data-a='{"select":"ob","mode":"save"}'>сохранить</button><br>
            <button class="btn a save" type="submit" data-a='{"select":"ob","mode":"purgeDB"}'>очистить БД</button>
    </div>
</form>
