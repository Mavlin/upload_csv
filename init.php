<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/SplClassLoader.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/handler.php';
if ( $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' ){
    ini_set('display_errors','off');
}

$req = null;
$query = null;
$arg = [];

if (empty($_GET)) $query = $_POST;
if (empty($_POST)) {
    $req = $_GET['req'];
    $query = json_decode($req, true);
}
$ns = 'Controllers';

$data = []; // массив данных для передачи в целевой класс
foreach ($query as $key => $value) //* в этом цикле собирается массив аргументов для передачи выбранному
{ //  методу класса = select/mode, имена ключей определяет клиент
    if ( $key == 'where' || $key == 'order' || $key == 'limit' || $key == 'data' )
        $data[$key] = $value;
}

$arg['_'.$query['mode'].'_'] = $data; // искомый метод в классе

$full_path = $ns . '\\' . $query['select'];
$ajaxRequest = new $full_path( $arg );
$ajaxRequest->encodeResponse();
$ajaxRequest->showResponse();
