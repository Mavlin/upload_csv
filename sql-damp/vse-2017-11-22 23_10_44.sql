-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных vse
CREATE DATABASE IF NOT EXISTS `vse` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vse`;

-- Дамп структуры для процедура vse.addObs
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `addObs`(
	IN `_Rows` INT















)
BEGIN
DECLARE i INT;
SET i = 0; 
truncate table tbl_ob_data;
WHILE i < _Rows DO 
insert into tbl_ob_data (ob_id,ob_date,ob_body,ob_contacts) values (null, NOW(), concat('Объявление #',i+1,' Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.'), 'Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я');
SET i = i + 1; 
END WHILE; 
insert LOW_PRIORITY into tbl_count set cnt=(select count(ob_id) from tbl_ob_data)
 ON DUPLICATE KEY UPDATE cnt=(select count(ob_id) from tbl_ob_data);
END//
DELIMITER ;

-- Дамп структуры для таблица vse.tbl_count
CREATE TABLE IF NOT EXISTS `tbl_count` (
  `cnt_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `cnt` int(11) DEFAULT NULL,
  UNIQUE KEY `key` (`cnt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='таблица хранит одно значение - общее кол-во записей в tbl_ob_data\r\nтаким образом предлагается решить проблему длительного подсчета общего кол-ва записей в таблицах InnoDB';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица vse.tbl_ob_data
CREATE TABLE IF NOT EXISTS `tbl_ob_data` (
  `ob_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ob_date` datetime DEFAULT NULL,
  `ob_body` text,
  `ob_contacts` text,
  PRIMARY KEY (`ob_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10178 DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура vse.updCountOb
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `updCountOb`(
	IN `_Rows` INT








































)
    COMMENT 'нужна для кэширования кол-ва записей, запускать руками если добавление записей происходит не через Web интерфейс'
BEGIN
insert LOW_PRIORITY into tbl_count set cnt=(select count(ob_id) from tbl_ob_data)
 ON DUPLICATE KEY UPDATE cnt=(select count(ob_id) from tbl_ob_data);
END//
DELIMITER ;

-- Дамп структуры для триггер vse.updCountOnDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `updCountOnDelete` AFTER DELETE ON `tbl_ob_data` FOR EACH ROW BEGIN
insert LOW_PRIORITY into tbl_count set cnt=(select count(ob_id) from tbl_ob_data)
 ON DUPLICATE KEY UPDATE cnt=(select count(ob_id) from tbl_ob_data);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
